package cn.yunhe.springmvc.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.core.NamedThreadLocal;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

/**
 * 性能监控-记录Controller方法执行时间
 * 
 * 注：最好把TimerInterceptor放在拦截器链的第一个，这样得到的处理时间才是比较准确的
 * 
 * @author YUNHE
 *
 */
public class TimerInterceptor implements HandlerInterceptor {

    /**
     * NamedThreadLocal：Spring提供的一个命名的ThreadLocal实现
     */
    private NamedThreadLocal<Long> startTimeThreadLocal = new NamedThreadLocal<Long>("WatchExecuteTime");

    /**
     * 控制器的方法被调用之前
     */
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		long beginTime = System.currentTimeMillis();//开始时间
        startTimeThreadLocal.set(beginTime);//线程绑定变量（该数据只有当前请求的线程可见）  
		return true;//继续流程 
	}

    /**
     * 控制器的方法被调用之后
     */
	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {

        long endTime = System.currentTimeMillis();//结束时间  
        long beginTime = startTimeThreadLocal.get();//得到线程绑定的局部变量（开始时间）  
        long consumeTime = endTime - beginTime;//消耗的时间  
        if(consumeTime > 500) {//此处认为处理时间超过500毫秒的请求为慢请求  
        	//TODO:记录到日志文件  
            System.out.println(String.format("%s 消耗 %d 毫秒", request.getRequestURI(), consumeTime));  
        }              
        
	}

    /**
     * 在DispatcherServlet 渲染了对应的视图之后执行。
     * 这个方法的主要作用是用于进行资源清理工作的
     * 
     * 该方法也是需要当前对应的Interceptor的preHandle方法的返回值为true时才会执行。
     * 该方法将在整个请求完成之后，也就是DispatcherServlet渲染了视图执行，
     * 这个方法的主要作用是用于清理资源的，
     * 当然这个方法也只能在当前这个Interceptor的preHandle方法的返回值为true时才会执行。
     * 也就是postHandle这个方法抛出异常，也会执行afterCompletion此方法
     * 
     */
	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		
	}

}
